const routes = [
  {
    path: "/",
    component: "home",
    exact: true,
  },
];

export default routes;
