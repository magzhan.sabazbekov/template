import { lazy, Suspense } from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import routes from "./config";

const PrivateRoute = ({ component: Component, ...rest }) => {
  const isHaveRoute = true;
  return (
    <Route
      {...rest}
      render={(props) => {
        return isHaveRoute ? <Component {...props} /> : <Redirect to={"/"} />;
      }}
    />
  );
};

const Router = () => {
  return (
    <>
      <Suspense fallback={null}>
        {/* <Header /> */}

        <Switch>
          {routes.map((routeItem) => {
            if (routeItem.private) {
              return (
                <PrivateRoute
                  key={routeItem.component}
                  path={routeItem.path}
                  exact={routeItem.exact}
                  component={lazy(() =>
                    import(`../modules/${routeItem.component}`)
                  )}
                />
              );
            } else {
              return (
                <Route
                  key={routeItem.component}
                  path={routeItem.path}
                  exact={routeItem.exact}
                  component={lazy(() =>
                    import(`../modules/${routeItem.component}`)
                  )}
                />
              );
            }
          })}
        </Switch>
      </Suspense>
    </>
  );
};

export default Router;
